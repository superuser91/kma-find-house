### Mọi người dùng:
+ Đăng ký và đăng nhập tài khoản.

+ Đăng nhập bằng Facebook hoặc Google.


### Người tìm trọ:

+ Tìm kiếm phòng theo vị trí, giá, diện tích. Sắp xếp kết quả tìm kiếm theo giá và diện tích.

+ Tìm kiếm nhà trọ theo bản đồ với bán kính tùy chọn.

+ Hiện thị toàn bộ nhà trọ trên bản đồ.

+ Thêm kết quả tìm kiếm vào danh mục yêu thích để dễ dàng tìm lại hoặc so sánh

+ Đăng ký làm người đăng bài.

+ Đặt phòng trọ.

+ Bình luận bài đăng.


### Chủ phòng trọ

+ Đăng bài mới.

+ Xem danh sách các bài đã đăng.

+ Sửa, xóa bài đã đăng.


### Quản trị viên

+ Phê duyệt bài đăng.

+ Phê duyệt yêu cầu đăng ký làm người đăng bài (chủ trọ).